twend.it-json
=============

A node.js API system for twend.it that gives the current trending topics for many areas.

Accessing format is like this:<br>
http://link:8000/current/world<br>
http://link:8000/current/us<br>
http://link:8000/current/uk<br>
http://link:8000/current/france<br>
http://link:8000/current/germany<br>

The script reaches to the related page of twend.it and parses it, then returns the trending topics in JSON format. The script can be altered to return the amount of time it remained as a trending topic too.

From the page above, one can see the region list and give area variables to the URL according to the subdomain names on twend.it.
<br>http://twend.it/world
