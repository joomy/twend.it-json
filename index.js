/*
fetching current trending topics from twend.it
an API system for twend.it
*/
var jsdom = require("jsdom");
var fs = require("fs");
var jquery = fs.readFileSync("jquery.js", "utf-8");
Array.prototype.remove = function(from, to) {
  var rest = this.slice((to || from) + 1 || this.length);
  this.length = from < 0 ? this.length + from : from;
  return this.push.apply(this, rest);
};
function isFunction(possibleFunction) {
  return (typeof(possibleFunction) == typeof(Function));
}

function twendFetch(area,callback) {
    if(area == "world") {
        area = "";
    } else {
        area = area + ".";
    }
    jsdom.env({
      url: "http://" + area + "twend.it/",
      src: [jquery],
      done: function (errors, window) {
        var $ = window.$;
        var incomingData = $("#current ~ script").html();
        incomingData = JSON.parse(((incomingData.split("var data = "))[1].split("var options"))[0].slice(0,-2));
        incomingData.remove(0);
        //console.log(incomingData);
        var returningList = [];
        incomingData.forEach(function(topic) {
            returningList.push(topic.label);   
        });
        if(isFunction(callback)) {
            callback(returningList);
        }
      }
    });
}

var express = require("express");
var app = express();

app.get("/", function(req,res) {
    res.send("This is a Twend.it current trending topics unofficial API service, which returns data in JSON format.");
});

app.get("/current/:area", function(req,res) {
    console.log("New request for " + req.url);
    var area = req.params['area'];
    twendFetch(area, function (list) {
        res.writeHead(200, { 'Content-Type': 'application/json' });
        res.write(JSON.stringify(list).toString("utf-8"));
        res.end();
    });
});

app.listen("8000", function() {
    console.log("Server running on port 8000");
    console.log("--------------------------");
});
